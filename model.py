import pandas as pd
import csv
import pdb
import functools
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession, Window
import pyspark.sql.functions as F
from pyspark.sql import DataFrame
import pyspark.sql.types as T
from pyspark.ml.feature import VectorAssembler, StandardScaler
from pyspark.ml import Pipeline
from pyspark.ml.regression import LinearRegression 
from pyspark.ml.evaluation import RegressionEvaluator

# spark-submit --driver-memory 2g model.py

spark = SparkSession.builder.appName("CovidPrediction").getOrCreate()
spark.sparkContext.setLogLevel("ERROR")

#TODO add back total count and scale features
def get_percent_error(predictions, is_ensemble):
  total_pct = 0
  for row in predictions.collect():
#row = predictions.loc[predictions['created_at'] == date_key]
    truth = row['avg(confirmed_cases)'] if is_ensemble else row['confirmed_cases']
    pred = row['avg(prediction)'] if is_ensemble else row['prediction']
    pct = abs(truth - pred) / pred
    total_pct += pct

  avg_pct = total_pct / predictions.count()
  return avg_pct
  

def ensemble(groups, data_type, case_type, days_ahead):
  dataset_name = "{}_{}_{}_{}".format("ensemble", data_type, case_type, days_ahead)
  filepaths = ["data_labelled/{}_{}_{}_{}.csv".format(group_name, data_type, case_type, days_ahead)
               for group_name in groups]
  datas = [spark.read.csv(filepath, inferSchema=True, header=True)
           for filepath in filepaths]

  strict_columns = ['total_count', 'covid_ratio', 'sentiment_avg', 'like_avg', 'retweet_avg', 'quote_avg', 'reply_avg']
  soft_columns = ['total_count', 'soft_covid_ratio', 'soft_sentiment_avg', 'soft_like_avg', 'soft_retweet_avg', 'soft_quote_avg', 'soft_reply_avg']
  feature_columns = strict_columns if data_type == "strict" else soft_columns
  vector_assembler = VectorAssembler(inputCols=feature_columns, outputCol="features")
  standard_scaler = StandardScaler(inputCol="features", outputCol="scaled_features")
  stages = [vector_assembler, standard_scaler] 
  pipeline = Pipeline().setStages(stages)

  def transform_data(data, pipeline):
#data = vector_assembler.transform(data)
#data = standard_scaler.fit(data).transform(data)
#return data
    pipelineModel = pipeline.fit(data)
    return pipelineModel.transform(data)

  def split(data):
    data = data.select(['created_at', 'scaled_features', 'confirmed_cases'])
    data = data.withColumn("rank", F.percent_rank().over(Window.partitionBy().orderBy("created_at")))
    train_data = data.where("rank <= .8").drop("rank")
    test_data = data.where("rank > .8").drop("rank")
    return (train_data, test_data)

  transformed_data = [transform_data(data, pipeline) for data in datas]
  split_data = [split(data) for data in transformed_data]

  # model training and prediction
  MLR = LinearRegression(featuresCol="scaled_features", labelCol="confirmed_cases")
  def predict(model, data_set):
    trained_model = MLR.fit(data_set[0])
    return trained_model.evaluate(data_set[1])

  #limits predictions by the minimum test data size
  min_test_size = min([data_set[1].count() for data_set in split_data])
  print("MINTESTSIZE:{}".format(min_test_size))
  predictions = [predict(MLR, dataset).predictions for dataset in split_data]
  agg_predictions = functools.reduce(DataFrame.union, predictions)
  predictions = agg_predictions.groupBy("created_at").avg().orderBy(F.desc("created_at")).limit(min_test_size)
  cleaned_predictions = predictions.select(['created_at', 'avg(confirmed_cases)', 'avg(prediction)'])
  cleaned_predictions.toPandas().to_csv("predictions/simple_{}.csv".format(dataset_name))

  # evaluating
  evaluation = RegressionEvaluator(labelCol="avg(confirmed_cases)", predictionCol="avg(prediction)")
  r2 = evaluation.evaluate(predictions, {evaluation.metricName: "r2"})
  rmse = evaluation.evaluate(predictions, {evaluation.metricName: "rmse"})
  percent_error = get_percent_error(predictions, True)

  return [dataset_name, min_test_size, r2, rmse, percent_error]


def evaluate(group_name, data_type, case_type, days_ahead):
  dataset_name = "{}_{}_{}_{}".format(group_name, data_type, case_type, days_ahead)
  filepath = "data_labelled/{}.csv".format(dataset_name)
  data = spark.read.csv(filepath, inferSchema=True, header=True).cache()

  strict_columns = ['total_count', 'covid_ratio', 'sentiment_avg', 'like_avg', 'retweet_avg', 'quote_avg', 'reply_avg']
  soft_columns = ['total_count', 'soft_covid_ratio', 'soft_sentiment_avg', 'soft_like_avg', 'soft_retweet_avg', 'soft_quote_avg', 'soft_reply_avg']
  feature_columns = strict_columns if data_type == "strict" else soft_columns

  # processing the data
  vector_assembler = VectorAssembler(inputCols=feature_columns, outputCol="features")
  standard_scaler = StandardScaler(inputCol="features", outputCol="scaled_features")
#data = vector_assembler.transform(data)
#data = standard_scaler.fit(data).transform(data)
  stages = [vector_assembler, standard_scaler] 
  pipeline = Pipeline().setStages(stages)
  pipelineModel = pipeline.fit(data)
  data = pipelineModel.transform(data)

  # splitting the data
  data = data.select(['created_at', 'scaled_features', 'confirmed_cases'])
  data = data.withColumn("rank", F.percent_rank().over(Window.partitionBy().orderBy("created_at")))
  train_data = data.where("rank <= .8").drop("rank")
  test_data = data.where("rank > .8").drop("rank")
  #(train_data, test_data) = data.randomSplit([0.8, 0.2])

  # model training and prediction
  MLR = LinearRegression(featuresCol="scaled_features", labelCol="confirmed_cases")
  model = MLR.fit(train_data)
  pred = model.evaluate(test_data)
  print("predictions for {}:".format(filepath))
  pred.predictions.show()
  cleaned_predictions = pred.predictions.select(['created_at', 'confirmed_cases', 'prediction'])
  cleaned_predictions.toPandas().to_csv("predictions/simple_{}.csv".format(dataset_name))

  # evaluating
  evaluation = RegressionEvaluator(labelCol="confirmed_cases", predictionCol="prediction")
  r2 = evaluation.evaluate(pred.predictions, {evaluation.metricName: "r2"})
  rmse = evaluation.evaluate(pred.predictions, {evaluation.metricName: "rmse"})
  percent_error = get_percent_error(cleaned_predictions, False)

  return [dataset_name, test_data.count(), r2, rmse, percent_error]


def write_metrics(metrics, filename):
  with open("results/{}".format(filename), "wb") as csv_file:
    writer = csv.writer(csv_file, delimiter=",")
    writer.writerows(metrics)


def main():
  ENSEMBLE_GROUPS = ["news", "prov_rep", "prov_gov", "health", "mp"]
  GROUPS = ENSEMBLE_GROUPS + ["all"]
  DATATYPES = ["soft", "strict"]
  CASETYPES = ["change", "total"]
  DAYSAHEAD = [0, 1, 3, 7, 14, 30]


  metrics = [["dataset", "test_size", "r2", "rmse", "percent_error"]]
  for group_name in GROUPS:
    for data_type in DATATYPES:
      for case_type in CASETYPES:
        for days_ahead in DAYSAHEAD:
          metrics.append(evaluate(group_name, data_type, case_type, days_ahead))

  for data_type in DATATYPES:
    for case_type in CASETYPES:
      for days_ahead in DAYSAHEAD:
        metrics.append(ensemble(ENSEMBLE_GROUPS, data_type, case_type, days_ahead))

  write_metrics(metrics, "simple_linear_regression.csv")


main()

