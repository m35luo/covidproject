# CS451 Final Project
You don't have to implement an entire thing at once. If you're patient, you can break it up into manageable pieces and even stop somewhere in the middle. This is a skill, to be able to leave something in a steady half-done state.

## TODO:
action before motivation!!!
- ingest the raw data before worrying about how to format it for models
  - twitter handle list
  - corona keywords
- sentiment analysis
- review big data data mining lectures
- figure out what models to use
- additional reading (if necessary)

## Questions
- is sentiment analysis too simple?
- what is the most valuable feature of predicting corona virus? reaction to lockdown
- how to address skew in twitter representation?
  - younger, social, more urban
  - stratify the population so that we can weigh different types of populations?
  - predict the amount of cases in the provinces that the tweets are actually representative of?
  - based on sentiment of replies... are they representative of the population?

## Main Idea
- using Twitter data, try to forecast future covid cases.
- the premise of the idea is the belief that twitter data is useful because it reflects the current attitudes the population. If the spread of covid is a result of the behavior of the population; and the behavior of the population is reflected from the peoples attitudes, then perhaps we can use the attitudes of the population to forecast the spread of covid. This means that the data I am using is specifically the responses to covid policy changes by the government.
- since data is king and the limitations of the Twitter api (or more accurately, the limitations of my wallet), I need to ingest the most interesting sample possible.
- More precisely, I will be specifically targetting the population's response to changing policies. I believe that the effectiveness government's changing policies is limited by the adoption of those policies by the population. So, I will focusing the dataset to 1. the government's changing policies and 2. the reaction to the changing policies.
- TODO justify why you didn't use and filter by geo-location? In accurate sample to get canadians because most people don't have their location enabled. Maybe americans follow similarly, but the lockdown policies seem to be different enough for canada.

## Method
- Since the ultimate goal is to predict the number of cases in Canada, each training data row will be limited to the amount of days that I can collect. I will be training and evaluating several models, where each model forecasts a different time horizon given the tweets from a given day. I'm taking this approach so that I can get the most accurate prediction possible from the data that I am collecting.
  - ideally i would be able to get replies through recent streaming as well
- each data row will be for a given day:
  - total number of tweets
  - average sentiment of those tweets
  - entities of the tweets?
  - current number of cases 
    - (maybe the model is best at predicting change, since I'm looking at the change in the people's responses to policy changes)
  - next day's number of cases
  - next week's number of cases
  - next 2 week's number of cases
  - next month's number of cases

## Implementation 1st Iteration
Basically just to set up the basic data pipeline architecture and make sure that I have a working prediction model

At a high level, there are four/five phases.

### 1. ingest data
Since I spent a significant amount of time on this, I should mention that I tried to use the premium search api before I realized that there just isnt enough data to make a significant analysis. The sample didn't seem significant enough and I feared that it would be too random. In other words, my wallet was a limitation.

Since I started late, I need to use the search api instead of the streaming api to get the data. Ideally, I would have started this project much earlier so that I would have been able to get more data and not be limited by the search api's total number of requests.

Since Twitter sets crazy rates for backwards search, I was unable to get the replies to the tweets and do a proper analysis on the replies. I could have sampled the tweets that I would search for the replies on a given day, but even if i took a random tweet from each day, I would still be limited by the number of days that I could get. Only 50 requests for tweets beyond 30 days. So, I accepted the tradeoff and instead used the user_timelines endpoint to grab the 3200 most recent tweets and group them by the day as well as categorize the source as either: news, public official, or health official.

I needed to cutoff the data by the second last date in which all the handlers had tweets so that the data is not skewed by a given source.

Before training the model, I split off the training and evaluation data by randomly choosing dates to evaluate.

For the presentation: handling the rate limits were a bit of a pain and creating a pager to loop through the requests to get as much data as possible.
- sleeping between requests


- get the relevant twitter handles
- check the results on the dashboard
- in testing, compare results of api with actual twitter app results
1. get timeline of official accounts(V1)
- what is the most recent latest day for all the twitter accounts?
- filter relevant data: created_at, full_text, id, user, user_group?
- presentation:
  - show list of twitter handles
  - this way, I am getting a _Canadian_ sample
2. hydrate the tweets
- add metrics: 

- how many days can I collect?
- data for each day:
  - number of coronavirus tweets from the public accounts, get as many relevant public accounts as possible
  - separate into symptom keyword count?
  - policy keywords?
  - recovery keyword

### 2. analyze and process data
process, and save
- does it make sense to batch the data together? what does a day represent?

- covid keywords: sick, safe, pandemic, covid, hospital
- aggregate for each day and collect the amount of tweets
- batch and aggregate
- cleaning the data:
  - regex to remove \n, RT, links
- filter out only relevant tweets to COVID
  - only doing sentiment analysis on the covid tweets
  - get relevant list of keywords to COVID
    - vaccine?
  - what the features?
    - sentiment, 

data features / day:
- would median be a helpful feature?
- corona tweet count
- corona tweet porportion
- corona sentiment average
- corona averages:
  - retweet/favorite ratio? bad if super high because people don't favorite want they don't "like", that is, reflects that they dont like it
  - retweet count
  - favoirte count
  - reply count
  - quote tweet count

- TODO curate data for different machine learning models?
  - does that mean to batch the data if we need?
  - what can I do to make it better?

### 3. enrich the data
- how to get sentiment score?
  - read twitter sentiment docs
  - different sentiment packages...
- entity extraction?
- how to add in the covid stats?
- add in kaggle john hopkins repo
  - dealing with empty values for province/state value
- TODO anything else?

### 4. train and evaluate machine learning models
- split the available data?
- pyspark and spark.mllib
- building model? what kind of model? how to train using spark.ml?
- is it possible to test what the model is best at predicting?
  - add in different time horizon of the number cases?
  - how to evaluate?
- the number of covid cases is what we're trying to predict, so does that mean that we have to batch the number of tweets? feature extracting from a match?
  - average sentiment? or just extract the fear sentiment? negative sentiment?
  - number of covid tweets?
  - other keywords like masks? social distancing? shortness of breath? hospital?
- using keywords as features? because tweets are best at reflecting the tweeter's health status and  mindset
    - how to filter out reactions vs indicators? how to filter out noise?


## Implementation 2nd Iteration
- try using different ML models
- integrate web scraping of news? what features can i even extract?
- use log log scale so that we can get a flat line to more easily fit and understand (use linear regression)
- can we account/smooth the days where cases skyrocket because of new testing?
- expanding the list of keywords to include more covid symptoms such as shortness of breath
- get sentiment relevant to news article response? skewed...
- consider adding getting public offical accounts/news accounts to determine policy change and reactions to policy changes: assign a special weight
  - consider separating into multiple samples -> similar to ensemble learning
  - figure out how to use twitter api to retrieve tweets from specific outlets
- additional data prep:
  - remove hashtag symbol and its content since they dont contribute to the meaning of the tweet in the analysis
  - remove RTs, replies, and users with more than 500 tweets (different population super posters/bots)
- additionally filter by geo location?
 

## Presentation
1. idea
- metrics to loosely determine the public response
2. methodology used
3. implementation
- we learned in class that data is king, so i went back and tweaked the data prep
- a lot of the work for most of the project was tinkering with the api and seeing which endpoints dont affect the total request limit (for > 7 days search). for example, get timeline instead of just doing a global search
4. results (evaluation methodology)
- maybe the government is not that reflective of the covid situation
- key point is to show research and iteration, what are the important parts that i discovered through iteration?
- recommend using twitter api v2 if possible, it has more information

- show appreciation for what i learned (makes me value stats, maybe i need to learn more):
  - normalizing, correcting for correlated features, different distributions
- can I publish a blog post on my personal website to help my job search in the upcoming year? I think its an interesting project, particularly because its relevant. 

## polish

## public accounts list:
total of 376 accounts
### national news:
CBCNews
ctvnationalnews
globalnews
HuffPostCanada
nationalpost
financialpost
vicecanada

### government officials:
since they are representative of the population, their level of concern should reflect level of concern of the people
of 338 public mps, i found 313 twitter accounts 
from https://www.ourcommons.ca/en/social-media

### provincial premieres and deputy premieres:
jjhorgan
carolejames
jkenney
LukaszukAB
premierscottmoe
gordwyant
mlastefanson
brianpallister
celliottability
fordnation
francoislegault
GGuilbaultCAQ
premierbhiggs
stephenmcneil
dennyking
premierofnl
siobhancoadynl
ranjpillai1
premier_silver
dianearchienwt
CCochrane_NWT
JSavikataaq

### provincial government accounts
BCGovNews
YourAlberta
SKGov
MBGov
ONgov
gouvqc (include? all french)
GovNL
Gov_NB
nsgov
infopei
yukongov
GOVofNUNAVUT

missing northwest territories

### health accounts (CDC, CPHO, Canada, minister of health):
- chief public health officer
- chief medical officer of health
cdnminhealth
CPHO_Canada/
safety_canada

phsaofbc
goahealth
hcs_govnl
saskhealth
nbhealth
nshealth
ONThealth
Health_PEI
jsjaylward
pfrostoldcrow
nthssa
hssyukon

cdcofbc
cmoh_alberta
drsusanshaw
roussin_brent
epdevilla
cmoh_nl
strangrobert
nwt_cpho

manitoba, nunavut doesnt have seperate health account
names are different, could have missed something, but not all the people have a twitter account

CMOH in provinces: chief medical officer for health



