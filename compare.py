import pandas as pd
import pdb
import csv

def write_metrics(metrics, filename):
  with open("results/{}".format(filename), "wb") as csv_file:
    writer = csv.writer(csv_file, delimiter=",")
    writer.writerows(metrics)

def main():
  file1 = pd.read_csv("results/simple_linear_regression.csv")
  file2 = pd.read_csv("results/simple_linear_regression_no_total_count.csv")

  ENSEMBLE_GROUPS = ["news", "prov_rep", "prov_gov", "health", "mp"]
  GROUPS = ENSEMBLE_GROUPS + ["all"]
  DATATYPES = ["soft", "strict"]
  CASETYPES = ["change", "total"]
  DAYSAHEAD = [0, 1, 3, 7, 14, 30]

  metrics = [["dataset", "r2", "rmse"]]
  for dataset in file1['dataset']:
    r2_1 = file1.loc[file1['dataset'] == dataset]['r2'].iloc[0]
    r2_2 = file2.loc[file2['dataset'] == dataset]['r2'].iloc[0]
    rmse_1 = file1.loc[file1['dataset'] == dataset]['rmse'].iloc[0]
    rmse_2 = file2.loc[file2['dataset'] == dataset]['rmse'].iloc[0]

    r2_cmp = "no_total" if r2_1 > r2_2 else "total"
    rmse_cmp = "no_total" if rmse_1 < rmse_2 else "total"
    metrics.append([dataset, r2_cmp, rmse_cmp])
    
  write_metrics(metrics, "compare_linear_total.csv")



# get the relevant files into dataframes
# compare each cell in the by iterating through the names and creating a new dataframe
# save new comparison dataframe to csv


main()
