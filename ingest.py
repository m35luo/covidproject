import json
import pdb
import time
import requests
import logging
import pandas as pd
import re

from textblob import TextBlob
from datetime import datetime, timedelta

from TwitterAPI import TwitterAPI, TwitterOAuth, TwitterRequestError, TwitterConnectionError

class Consumer:
    def __init__(self):
        try:
            self.auth = TwitterOAuth.read_file('credentials.txt')
            self.api = TwitterAPI(self.auth.consumer_key,
                                  self.auth.consumer_secret,
                                  self.auth.access_token_key,
                                  self.auth.access_token_secret,
                                  api_version='2')

        except TwitterRequestError as e:
            print(e.status_code)
            for msg in iter(e):
                print(msg)

        except TwitterConnectionError as e:
            print(e)
        
        except Exception as e:
            print(e)
   

    def get_timeline_tweet_ids(self, screen_name):
        raw_filename = "data_ingested/{}_timeline_raw.json".format(screen_name)
        ids_filename = "data_ingested/{}_timeline_ids.json".format(screen_name)

        params = {
            'screen_name': screen_name, 
            'count': 200,
            'include_rts': False,
            'trim_user': True
        }
        raw_timeline = self.get_raw_timeline(params)
        with open(raw_filename, 'w') as json_file:
            json.dump(raw_timeline, json_file, indent=2)

        tweet_ids = list(map(lambda t: t['id'], raw_timeline))

        with open(ids_filename, 'w') as json_file:
            json.dump(tweet_ids, json_file, indent=2)

        return tweet_ids

    def get_raw_timeline(self, request_params):
        full_timeline = []
        request_count = 0 # limit 
        while True:
            r = self.api.request('statuses/user_timeline', 
                                 params=request_params,
                                 api_version='1.1')
            
            json_response = r.json()
            request_count += 1
            item_count = len(json_response)
            full_timeline += json_response

            # 3200 / 200 per request = 16
            if request_count >= 16 or item_count == 0:
                break

            request_params['max_id'] = json_response[-1]['id']

            time.sleep(1)
                    
        return full_timeline

    def hydrate_timeline(self, tweet_ids, screen_name):
        # split into groups of n
        def batches(lst, n):
            for i in range(0, len(lst), n):
                yield lst[i:i + n]

        hydrated_timeline = []
        tweet_batches = list(batches(tweet_ids, 100))
        for tweet_batch in tweet_batches:
            hydrated_batch = self.hydrate_tweet_batch(tweet_batch)
            hydrated_timeline += hydrated_batch
            time.sleep(1)

        hydrated_timeline_filename = "data_ingested/{}_timeline_hydrated.json".format(screen_name)
        with open(hydrated_timeline_filename, 'w') as json_file:
            json.dump(hydrated_timeline, json_file, indent=2)

        return hydrated_timeline


    # only 100 are allowed at a time
    # consider changing created at to date with day so that we can group more easily later
    def hydrate_tweet_batch(self, tweet_ids):
        tweet_ids_str = ",".join(str(tweet_id) for tweet_id in tweet_ids)
        # TODO consider entities
        request_params = {
            'ids': tweet_ids_str,
            'tweet.fields': 'created_at,public_metrics,lang'
        }
        req = self.api.request("tweets", params=request_params)

        def flatten_public_metrics(tweet_object):
            flattened_tweet = {}
            for tweet_attr, tweet_val in tweet_object.items():
                if tweet_attr == 'public_metrics':
                    for metric_attr, metric_value in tweet_object[tweet_attr].items():
                        flattened_tweet[metric_attr] = metric_value
                else:
                    flattened_tweet[tweet_attr] = tweet_val

            return flattened_tweet

        req_json = req.json()
        return list(map(flatten_public_metrics, req_json['data']))
        

    def get_mp_accounts(self):
        # https://twitter.com/i/lists/864088912087715840/members?lang=en
        request_params = {
            'list_id': 864088912087715840,
            'count': 5000,
        }

        r = self.api.request("lists/members", params=request_params, api_version='1.1')
        members_json = r.json()
        with open("data_ingested/mp_screen_names_raw.json", 'w') as json_file:
            json.dump(members_json['users'], json_file, indent=2)

        return members_json

def initialize_logger():
    logger = logging.getLogger("logging_tryout2")
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter("%(asctime)s;%(levelname)s;%(message)s",
                                  "%Y-%m-%d %H:%M:%S")

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

    return logger

def download_hydrated_timelines(screen_names, consumer, logger):
    for screen_name in ALL_SCREEN_NAMES: 
        logger.info("--- start downloading raw timeline {}".format(screen_name))
        tweet_ids = consumer.get_timeline_tweet_ids(screen_name)
        logger.info("--- done downloading raw timeline {}".format(screen_name))

        logger.info("--- start downloading hydrated timeline {}".format(screen_name))
        hydrated_timeline = consumer.hydrate_timeline(tweet_ids, screen_name)
        logger.info("--- done downloading hydrated timeline {}".format(screen_name))

# assumes hydrated timelines exist for each of the screen_names
# cleans date, tweet text and filters by lang and reasonable dates
def filter_and_clean(screen_names, logger):
    cleaned_count = 0
    #remove text in the tweet that would affect the sentiment analysis
    def clean_text(text):
        text = re.sub(r'@[A-Za-z0-9]+', '', text)
        text = re.sub(r'#', '', text)
        text = re.sub(r'RT[\s]+', '', text)
        text = re.sub(r'https?:\/\/\S+', '', text)
        text = re.sub("\n", '', text)

        return text

    def clean_tweet(tweet):
        tweet_date = datetime.strptime(tweet['created_at'], "%Y-%m-%dT%H:%M:%S.%fZ")
        tweet['created_at'] = tweet_date.strftime("%Y-%m-%d")
        min_date = datetime(2020, 3, 11) # when nba season got canceled bc covid :P
        if tweet['lang'] == 'en' and tweet_date > min_date:
            del tweet['lang']
            tweet['text'] = clean_text(tweet['text'])
            return tweet


    for screen_name in screen_names:
        logger.info("--- start cleaning {}'s timeline".format(screen_name))
        with open("data_ingested/{}_timeline_hydrated.json".format(screen_name)) as json_file:
            hydrated_timeline = json.load(json_file)
        
        cleaned_tweets = map(clean_tweet, hydrated_timeline)
        cleaned_tweets = [tweet for tweet in cleaned_tweets if tweet is not None]
        cleaned_count += len(cleaned_tweets)
        with open("data_cleaned/{}_timeline_cleaned.json".format(screen_name), 'w') as processed_file:
            json.dump(cleaned_tweets, processed_file, indent=2)

        logger.info("--- done cleaning {}'s timeline".format(screen_name))

    return cleaned_count

def enrich(screen_names, logger):
    def add_sentiment(tweet):
        tweet['sentiment'] = TextBlob(tweet['text']).sentiment.polarity
        return tweet

    def add_is_covid(tweet):
        COVID_KEYWORDS = [
            "coronavirus", "corona", "wuhanvirus", "outbreak", "epidemic", "pandemic",
            "covid19", "covid-19", "covid", "sars-cov-2", "sarscov2", "chinavirus",
            "wuhan virus", "china virus", "sars"
        ]
        has_covid_keyword = False
        for keyword in COVID_KEYWORDS:
            if re.search(keyword, tweet['text'], re.IGNORECASE) is not None:
                has_covid_keyword = True
                break

        tweet['is_covid'] = has_covid_keyword
        if tweet['is_covid']:
            tweet['is_covid_soft'] = True
            return tweet

        COVID_SOFT_KEYWORDS = [
            "outbreak", "epidemic", "pandemic", "stayathome", "stay at home", "isolate",
            "mask", "stayhome", "washyourhand", "hand washing", "patient", "lockdown", "vaccine",
            "ppe", "asymptomatic", "social distancing", "herd immunity", "quarantine", "subsidy",
            "crisis"
        ]
        has_covid_soft_keyword = False
        for keyword in COVID_SOFT_KEYWORDS:
            if re.search(keyword, tweet['text'], re.IGNORECASE) is not None:
                has_covid_soft_keyword = True
                break

        tweet['is_covid_soft'] = has_covid_soft_keyword
        return tweet

    for screen_name in screen_names:
        logger.info("--- start enriching {}'s timeline".format(screen_name))
        with open("data_cleaned/{}_timeline_cleaned.json".format(screen_name)) as json_file:
            cleaned_timeline = json.load(json_file)
        
        enriched_tweets = map(add_sentiment, cleaned_timeline)
        enriched_tweets = list(map(add_is_covid, enriched_tweets))
        with open("data_enriched/{}_timeline_enriched.json".format(screen_name), 'w') as enriched_file:
            json.dump(enriched_tweets, enriched_file, indent=2)

        logger.info("--- done cleaning {}'s timeline".format(screen_name))

# removes id, text columns and adds new columns
def simple_aggregate(data):
    columns = [
        'total_count', 'covid_count', 'soft_covid_count',
        'retweet_sum', 'reply_sum', 'like_sum', 'quote_sum', 'sentiment_sum',
        'soft_retweet_sum', 'soft_reply_sum', 'soft_like_sum', 'soft_quote_sum', 'soft_sentiment_sum'
    ]
    new_data = dict.fromkeys(columns, 0)
    new_data['total_count'] = data['id'].count()
    # TODO sentiment sums returning a list of values?
    for _, row in data.iterrows():
        if row['is_covid']:
            new_data['covid_count'] += 1
            new_data['retweet_sum'] += row['retweet_count']
            new_data['reply_sum'] += row['reply_count']
            new_data['like_sum'] += row['like_count']
            new_data['quote_sum'] += row['quote_count']
            new_data['sentiment_sum'] += row['sentiment']

        if row['is_covid_soft']:
            new_data['soft_covid_count'] += 1
            new_data['soft_retweet_sum'] += row['retweet_count']
            new_data['soft_reply_sum'] += row['reply_count']
            new_data['soft_like_sum'] += row['like_count']
            new_data['soft_quote_sum'] += row['quote_count']
            new_data['soft_sentiment_sum'] += row['sentiment']

    return pd.Series(new_data, index=columns)

def get_max_min_date(screen_names):
    max_min_date = pd.Timestamp('2020-03-11')
    min_screen_name = ""
    for screen_name in screen_names:
        full_df = pd.read_json("data_enriched/{}_timeline_enriched.json".format(screen_name))
        if full_df.size == 0:
            continue

        created_at = full_df['created_at']
        created_at = created_at.groupby(created_at)
        minimum_date = created_at.min().min() # sorts then gets minimum
        if minimum_date > max_min_date:
            max_min_date = minimum_date
            min_screen_name = screen_name

    print("debug: MIN_SCREEN_NAME: {}, MIN_DATE: {}".format(min_screen_name, max_min_date))
    return max_min_date

def prepare_all_training_data(groups):
    for group in groups:
        prepare_training_data(*group)

def prepare_training_data(group_name, screen_names):
    max_min_date = get_max_min_date(screen_names)
    max_min_date_str = max_min_date.strftime("%Y-%m-%d")
    print("---debug MAX_MIN_DATE_STR: {}, GROUP: {}".format(max_min_date_str, group_name))

    all_dfs = []
    for screen_name in screen_names:
        all_dfs.append(pd.read_json("data_enriched/{}_timeline_enriched.json".format(screen_name)))

    full_df = pd.concat(all_dfs)
    full_df = full_df.groupby('created_at').apply(simple_aggregate)
    full_df = full_df[full_df.index > max_min_date_str]
    full_df.to_csv("data_training/{}_rawtotals_unlabelled.csv".format(group_name))

    # replacing with ratios
    full_df = full_df.assign(covid_ratio=lambda row: row['covid_count'] / row['total_count'])
    full_df = full_df.assign(sentiment_avg=lambda row: row['sentiment_sum'] / row['total_count'])
    full_df = full_df.assign(like_avg=lambda row: row['like_sum'] / row['total_count'])
    full_df = full_df.assign(retweet_avg=lambda row: row['retweet_sum'] / row['total_count'])
    full_df = full_df.assign(quote_avg=lambda row: row['quote_sum'] / row['total_count'])
    full_df = full_df.assign(reply_avg=lambda row: row['reply_sum'] / row['total_count'])

    full_df = full_df.assign(soft_covid_ratio=lambda row: row['soft_covid_count'] / row['total_count'])
    full_df = full_df.assign(soft_sentiment_avg=lambda row: row['soft_sentiment_sum'] / row['total_count'])
    full_df = full_df.assign(soft_like_avg=lambda row: row['soft_like_sum'] / row['total_count'])
    full_df = full_df.assign(soft_retweet_avg=lambda row: row['soft_retweet_sum'] / row['total_count'])
    full_df = full_df.assign(soft_quote_avg=lambda row: row['soft_quote_sum'] / row['total_count'])
    full_df = full_df.assign(soft_reply_avg=lambda row: row['soft_reply_sum'] / row['total_count'])

    full_avg_df = full_df[[
        'total_count', 'covid_ratio', 'soft_covid_ratio', 'sentiment_avg', 
        'like_avg', 'retweet_avg', 'quote_avg', 'reply_avg', 'soft_sentiment_avg', 
        'soft_like_avg', 'soft_retweet_avg', 'soft_quote_avg', 'soft_reply_avg'
    ]]
    strict_avg_df = full_avg_df[[
        'total_count', 'covid_ratio', 'sentiment_avg', 
        'like_avg', 'retweet_avg', 'quote_avg', 'reply_avg' 
    ]]
    soft_avg_df = full_avg_df[[
        'total_count', 'soft_covid_ratio', 'soft_sentiment_avg', 
        'soft_like_avg', 'soft_retweet_avg', 'soft_quote_avg', 'soft_reply_avg'
    ]]

    full_avg_df.to_csv("data_training/{}_full_avg_unlabelled.csv".format(group_name))
    strict_avg_df.to_csv("data_training/{}_strict_avg_unlabelled.csv".format(group_name))
    soft_avg_df.to_csv("data_training/{}_soft_avg_unlabelled.csv".format(group_name))
    print("---debug DONE preparing training data for {}".format(group_name))


def label_all_data(groups):
    for group in groups:
        for file_type in ["soft", "strict", "full"]:
            for days_ahead in [0, 1, 3, 7, 14, 30]:
                add_label(group[0], file_type, days_ahead)


# saves labelled data for change and total cases in separate files
def add_label(group_name, file_type, days_ahead):
    unlabelled = pd.read_csv("data_training/{}_{}_avg_unlabelled.csv".format(group_name, file_type))
    covid_confirmed = pd.read_csv("time_series_covid_confirmed_canada.csv", index_col=0)

    # cutoff max date for labelling
    max_date = datetime(2020, 12, 11) - timedelta(days=days_ahead)
    unlabelled_dates = unlabelled['created_at'].apply(lambda d: datetime.strptime(d, "%Y-%m-%d"))
    mask = (unlabelled_dates <= max_date)
    unlabelled = unlabelled.loc[mask]

    def format_date(date_str, days_ahead):
        t = datetime.strptime(date_str, "%Y-%m-%d")
        t += timedelta(days=days_ahead)
        return t.strftime("%-m/%-d/%y")
    
    def get_confirmed(confirmed, case_type, date_str, days_ahead):
        return confirmed[case_type][format_date(date_str, days_ahead)]

    cases_total_column = unlabelled['created_at'].apply(lambda d: get_confirmed(covid_confirmed, "total", d, days_ahead))
    cases_change_column = unlabelled['created_at'].apply(lambda d: get_confirmed(covid_confirmed, "change", d, days_ahead))

    labelled_total = unlabelled.assign(confirmed_cases=cases_total_column)
    labelled_change = unlabelled.assign(confirmed_cases=cases_change_column)

    labelled_total.to_csv("data_labelled/{}_{}_{}_{}".format(group_name, file_type, "total", days_ahead))
    labelled_change.to_csv("data_labelled/{}_{}_{}_{}".format(group_name, file_type, "change", days_ahead))


def get_confirmed_change_and_total():
    first_date_str = "1/22/20"
    confirmed_total = pd.read_csv("time_series_covid_confirmed_canada_total.csv")
    confirmed_total = confirmed_total.sum() # this becomes a series
    del confirmed_total['Country/Region']
    total_dict = confirmed_total.to_dict()
    change_dict = {}
    for key, val in total_dict.items():
        if key == first_date_str: # no previous day
            continue

        dateobj = datetime.strptime(key, "%m/%d/%y")
        dateobj -= timedelta(days=1)
        prev_date = dateobj.strftime("%-m/%-d/%y")
        change_dict[key] = val - total_dict[prev_date]

    del total_dict[first_date_str]
    merged_dict = {'total': total_dict, 'change': change_dict}
    merged_df = pd.DataFrame.from_dict(merged_dict)
    merged_df.to_csv("time_series_covid_confirmed_canada.csv")


def main():
    consumer = Consumer()
    logger = initialize_logger()

    NEWS_SCREEN_NAMES = ["CBCNews", "ctvnationalnews", "globalnews", "HuffPostCanada", "nationalpost", "financialpost", "vicecanada"]
    PROV_REP_SCREEN_NAMES = [
        "jjhorgan", "carolejames", "jkenney", "LukaszukAB", "premierscottmoe", "gordwyant", "mlastefanson", "brianpallister", "celliottability",
        "fordnation", "francoislegault", "GGuilbaultCAQ", "premierbhiggs", "stephenmcneil", "dennyking", "premierofnl", "siobhancoadynl",
        "ranjpillai1", "premier_silver", "dianearchienwt", "CCochrane_NWT", "JSavikataaq"
    ]
    PROV_GOV_SCREEN_NAMES = ["BCGovNews", "YourAlberta", "SKGov", "MBGov", "ONgov", "GovNL", "Gov_NB", "nsgov", "infopei", "yukongov", "GOVofNUNAVUT"]
    HEALTH_SCREEN_NAMES = [
        "cdnminhealth", "CPHO_Canada", "safety_canada", "phsaofbc", "goahealth", "hcs_govnl", "saskhealth", "nbhealth", "nshealth",
        "ONThealth", "Health_PEI", "jsjaylward", "pfrostoldcrow", "nthssa", "hssyukon", "cdcofbc", "cmoh_alberta", "drsusanshaw",
        "roussin_brent", "epdevilla", "cmoh_nl", "strangrobert", "nwt_cpho"
    ]
    raw_mp_accounts = []
    with open("data_ingested/mp_screen_names_raw.json") as json_file:
        raw_mp_accounts = json.load(json_file)
    MP_SCREEN_NAMES = list(map(lambda mp: mp['screen_name'], raw_mp_accounts))
    with open("mp_screen_names.json", 'w') as json_file:
        json.dump(MP_SCREEN_NAMES, json_file)

    ALL_SCREEN_NAMES = NEWS_SCREEN_NAMES + PROV_REP_SCREEN_NAMES + PROV_GOV_SCREEN_NAMES + HEALTH_SCREEN_NAMES + MP_SCREEN_NAMES

    GROUPS = [
        ("news", NEWS_SCREEN_NAMES), ("prov_rep", PROV_REP_SCREEN_NAMES),
        ("prov_gov", PROV_GOV_SCREEN_NAMES), ("health", HEALTH_SCREEN_NAMES),
        ("mp", MP_SCREEN_NAMES), ("all", ALL_SCREEN_NAMES)
    ]

    try:
        # 1. ingest: download hydrated timelines for all accounts
        #download_hydrated_timelines(ALL_SCREEN_NAMES, consumer, logger)

        # 2. filter and clean for all users
        #total_count = filter_and_clean(ALL_SCREEN_NAMES, logger)

        # 3. enrich the data: sentiment analysis
        #enrich(ALL_SCREEN_NAMES, logger)

        # 4. do date analysis and preprocessing to get unlabelled training data for each group
        # prepare_all_training_data(GROUPS)

        # 5. add label data
        #label_all_data(GROUPS)

        # TODO model prep! do simple linear regression first?
        # - normalize is "augmenting" the data & so is PCA
        # - the data is not linear by checking residual error of linear regression
        #   on choosing model (speed vs accuracy): https://miro.medium.com/max/753/0*XitqTqJMytRRsCFL.png
        # - flexiblity and interpretability tradeoff (linear -> SVM)
        #   - SVM requires a lot of tuning as well
        # - ARIMA/time series data? what happens now depends on the past and not the future? the time tells us a lot
        #   about the current context; depends more on recent past than distant past
        # - shove it in a linear regression first! normalize data and add values
        # - apply PCA? principle component analysis to avoid overfitting data; reduce dimensionality of feature space (metrics)
        # - do we need to split the train and test set in sequence? is it a time series data? well the 
        #   curve is non linear...
        # - normalize data? pyspark scalers. depends on model

        # 6. train!


    except Exception as e:
        print("ERROR OCCURED!!!")
        print(e)



main()
